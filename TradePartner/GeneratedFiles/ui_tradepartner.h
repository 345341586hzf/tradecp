/********************************************************************************
** Form generated from reading UI file 'tradepartner.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TRADEPARTNER_H
#define UI_TRADEPARTNER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TradePartnerClass
{
public:
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QDockWidget *dockWidget;
    QWidget *dockWidgetContents;

    void setupUi(QMainWindow *TradePartnerClass)
    {
        if (TradePartnerClass->objectName().isEmpty())
            TradePartnerClass->setObjectName(QString::fromUtf8("TradePartnerClass"));
        TradePartnerClass->resize(775, 504);
        centralWidget = new QWidget(TradePartnerClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        TradePartnerClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(TradePartnerClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 775, 23));
        TradePartnerClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(TradePartnerClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        TradePartnerClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(TradePartnerClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        TradePartnerClass->setStatusBar(statusBar);
        dockWidget = new QDockWidget(TradePartnerClass);
        dockWidget->setObjectName(QString::fromUtf8("dockWidget"));
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QString::fromUtf8("dockWidgetContents"));
        dockWidget->setWidget(dockWidgetContents);
        TradePartnerClass->addDockWidget(static_cast<Qt::DockWidgetArea>(1), dockWidget);

        retranslateUi(TradePartnerClass);

        QMetaObject::connectSlotsByName(TradePartnerClass);
    } // setupUi

    void retranslateUi(QMainWindow *TradePartnerClass)
    {
        TradePartnerClass->setWindowTitle(QApplication::translate("TradePartnerClass", "TradePartner", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TradePartnerClass: public Ui_TradePartnerClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TRADEPARTNER_H
