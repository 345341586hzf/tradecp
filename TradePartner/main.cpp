#include "tradepartner.h"
#include <QtWidgets/QApplication>
#include <qtplugin>

//  开发阶段使用动态链接,  发不阶段使用静态链接
#ifndef _DEBUG_MD

Q_IMPORT_PLUGIN(QWindowsIntegrationPlugin)
#pragma comment(lib, "version.lib")
#pragma comment(lib, "Netapi32.lib")
#pragma comment(lib, "userenv.lib")
#pragma comment(lib, "Wtsapi32.lib")
#pragma comment(lib, "Dwmapi.lib")
#pragma comment(lib, "Qt5WindowsUIAutomationSupportd.lib")
#endif

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	TradePartner w;
	w.show();
	return a.exec();
}
